# DB Backup

Backs up a Postgres DB to S3, run as a cron job.

Modified from:
https://jmrobles.medium.com/how-backup-your-postgres-db-into-aws-s3-in-kubernetes-edf6cf0db11
