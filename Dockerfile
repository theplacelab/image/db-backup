FROM postgres:latest
RUN apt-get update && \
    apt-get install -y mcrypt curl awscli
COPY do-backup.sh /do-backup.sh
RUN chmod +x /do-backup.sh