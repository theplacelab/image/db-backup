#!/bin/bash
curl -s -4 https://icanhazip.com/
export DUMP_FILE="$DATABASE_DB"_`date +%Y%m%d_%H%M%S`.pgdump
PGPASSWORD=$DATABASE_PASS pg_dump -d $DATABASE_DB -U $DATABASE_USERNAME -h $DATABASE_SERVER -p $DATABASE_PORT -f $DUMP_FILE
bzip2 $DUMP_FILE
mcrypt ${DUMP_FILE}.bz2 -k $DATABASE_PASS
AWS_ACCESS_KEY_ID=$DB_AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$DB_AWS_SECRET_ACCESS_KEY aws s3 cp ${DUMP_FILE}.bz2.nc $DB_S3_BACKUP_PATH --region=$DB_AWS_DEFAULT_REGION --endpoint-url=https://s3.wasabisys.com 
rm ${DUMP_FILE}.bz2
rm ${DUMP_FILE}.bz2.nc